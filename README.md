# python-kr2 (CSV Reader by Kosorev)



## Usage
### To run script use:
```
$ python3 work_with_csv.py <command> <arguments>

```    
### For example:
### 1. Display .csv file line by line 
```
$ python3 work_with_csv.py visualize -p test1.csv -r 5

```
### Or:
```
$ python3 work_with_csv.py visualize --path-to-csv test1.csv --rows-quantity 5

```

#### OUTPUT:
```
Row 1: 1;2;3
        1
        2
        3
Row 2: "a;"b;"c;d,e";f
        a
        b
        c;d,e
        f
Row 3: "word_1;"word_2;word_3";word_4;"word5;word6";word7"
        word_1
        word_2;word_3
        word_4
        word5;word6
        word7
Row 4: g;h;i;j
        g
        h
        i
        j

```

### 2. Get summarize content of some summable .csv files
```
$ python3 work_with_csv.py summarize test2.csv test3.csv test4.csv

```

#### OUTPUT:
```
[14, 15, 16]

```