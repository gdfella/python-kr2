import argparse
import sys


class ReadCSV:
    def __init__(self,
                 filename: str):
        """Конструктор класса ReadCSV, построчно считывающий исходный файл
        @param filename: str, - путь до исходного CSV-файла
        @raises RuntimeError в случае проблем с чтением исходного файла
        @raises BufferError с выводом сообщения 'no rows in CSV' в случае, если исходный файл пуст

        Объект класса должен иметь 2 атрибута
        - _rows: list, - список считанных строк исходного файла
        - is_summable: bool, - индикатор суммируемости исходного файла
            == True, если исходный CSV-файл состоит из только 1-го столбца,
                     причём содержит только целые числа
            == False, во всех остальных ситуациях

        В случае, если возникают проблемы с чтением исходного CSV-файла, требуется генерировать
        исключение RuntimeError
        В случае отсутствия строк в исходном файле, требуется генерировать исключение BufferError
        с сообщением 'no rows in CSV'

        ВАЖНО отметить, что в считываемых строках исходного CSV-файла требуется обрезать начальные
        и конечные пробельные символы
        """
        self._rows = []
        flag = True
        try:
            with open(filename, 'r') as file:
                for line in file:
                    self._rows.append(line.strip('\n').strip(' '))
                    if not line.strip('\n').strip(' ').isdigit():
                        flag = False
                if not self._rows:
                    raise BufferError('no rows in CSV')
        except (FileNotFoundError, PermissionError):
            raise RuntimeError
        self.is_summable = flag
        # print(self._rows, self.is_summable)

    def __getitem__(self,
                    index: int) -> str:
        """Оператор индексирования по строкам исходного CSV-файла
        @param index: int, - индекс строки в исходном CSV-файле
        @return: str, - искомая строка из входного CSV-файла
        """
        return self._rows[index]

    def __len__(self):
        return len(self._rows)

    def __add__(self, other):
        """Оператор сложения 2-х суммируемых (значение True у атрибута is_summable) CSV-файлов
        @param other: ReadCSV, - объект класса ReadCSV (считанный CSV-файл)
        @return: ReadCSV, - объект класса ReadCSV, представляющий собой сумму 2-х CSV-файлов
        @raises ArithmeticError с текстом 'can't summarize non-summable files' в случае,
            если сложить 2 исходных CSV-файла корректно нельзя (хотя бы один из них не-суммируемый)

        Результатом сложения 2-х суммируемых CSV-файлов является объект класса ReadCSV,
        длина атрибута _rows которого равна наименьшей длине атрибута _rows у складываемых файлов,
        а значения на соответствующих позициях равны сумме значений на тех же позициях
        в исходных файлах, т.е. чтобы получить 2-ой элемент атрибута _rows результирующего объекта,
        требуется сложить 2-ой элемент атрибута _rows объектов self и other
        В случае, если один из файлов не является суммируемым, генерировать
        исключение ArithmeticError с текстом 'can't summarize non-summable files'
        """
        if not (self.is_summable and other.is_summable):
            raise ArithmeticError("can't summarize non-summable files")
        self._rows = [int(self[i]) + int(other[i]) for i in range(min(len(self), len(other)))]
        return self

    def get_value(self,
                  row_index: int,
                  column_index: int) -> str:
        """Получение строкового значения в соответствующей ячейке входного CSV-файла
        @param row_index: int, - индекс строки CSV-файла
        @param column_index: int, - индекс столбца CSV-файла
        @return: str, - строковое значение в соответствующей ячейке
        @raises: RuntimeError в случае ошибок индексирования ячейки
        @raises: RuntimeError в случае отсутствия парных закрывающихся двойных ковычек

        Для получения значения ячейки в соответствующей строке исходного CSV-файла требуется
        разбить её с учётом того, что разделителем является символ ';'

        В случае ошибок индексирования по строкам или столбцам исходного CSV-файла
        генерировать исключение RuntimeError

        В строке могут присутствовать двойные кавычки: подстрока, обрамлённое с 2-х сторон
        двойными кавычками (символ ") должно интерпретироваться как одно значение
        В случае, если в строке для какой-то из открывающихся двойных ковычек отсутствует
        её закрывающаяся пара, генерировать исключение RuntimeError
        """

        try:
            line = self[row_index]
            if line.count("\"") % 2 == 1:
                raise RuntimeError("unavailable amount of [ \" ]")

            tmp = line.split('\"') if line.count('\"') != 0 else line.split(';')
            tmp = [i.strip(';') for i in tmp if i]

            return tmp[column_index]

        except IndexError:
            raise RuntimeError("incorrect index")

    def get_row(self, ind: int) -> list:
        line = self[ind]
        if line.count("\"") % 2 == 1:
            raise RuntimeError("unavailable amount of [ \" ]")

        tmp = line.split('\"') if line.count('\"') != 0 else line.split(';')
        tmp = [i.strip(';') for i in tmp if i]

        return tmp

    def get_rows(self):
        return self._rows


def parse_cmd_args(cmd_args: list) -> argparse.Namespace:
    """Разбор аргументов командной строки
    @param cmd_args: list, - список аргументов командной строки
    @return: argparse.Namespace, -
        subparser_name: str, - название режима работы скрипта ("визуализация" или "суммирование")
            - visualize:
                csv_filename: str, - путь до исходного CSV-файла
                rows_quantity: int, - ограничение на количество выводимых строк исходного CSV-файла
            - summarize:
                csv_filenames: list, - список путей до суммируемых CSV-файлов
    """
    parser = argparse.ArgumentParser(
        add_help=True,
        description="CSV Reader",
        formatter_class=argparse.HelpFormatter,
        epilog='''Additional information:\n\tI don't know what should i write here''')

    subparser = parser.add_subparsers(dest='command')
    summarize = subparser.add_parser('summarize')
    visualize = subparser.add_parser('visualize')

    visualize.add_argument(
        '-p',
        '--path-to-csv',
        dest='path',
        type=str,
        required=True
    )
    visualize.add_argument(
        '-r',
        '--rows-quantity',
        dest='rows',
        type=int,
        required=True
    )
    summarize.add_argument('paths', metavar='', nargs='*')

    return parser.parse_args(cmd_args)


def summarize_csvs(csv_filenames: list) -> list:
    """Суммирование целочисленных значений единственного столбца входных CSV-файлов
    @param csv_filenames: list, - список путей до CSV-файлов (хотя бы 1)
    @return: list, - список целых чисел с результатом сложения 1-го столбца CSV-файлов
    """
    readers = [ReadCSV(path) for path in csv_filenames]
    for i in range(1, len(readers)):
        readers[0] += readers[i]
    return readers[0].get_rows()


def visualize_csv(csv_filename: str,
                  rows_quantity: int) -> None:
    """Визуализация данных из входного CSV-файла
    @param csv_filename: str, - путь до CSV-файла
    @param rows_quantity: int, - количество строк для чтения из входного файла
    """
    count = 0
    reader = ReadCSV(csv_filename)
    while count < rows_quantity and count < len(reader):
        print(f"Row {count + 1}: {reader[count]}")
        for i in reader.get_row(count):
            print(f'\t{i}')
        count += 1


def run(args):
    if args.command == 'visualize':
        reader = ReadCSV(args.path)
        visualize_csv(args.path, int(args.rows))

    if args.command == 'summarize':
        print(summarize_csvs(args.paths))


def main():
    cli_args = sys.argv[1::]
    args = parse_cmd_args(cli_args)
    return run(args)


if __name__ == '__main__':
    main()
